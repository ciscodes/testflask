from flask import Flask, request, jsonify
from flask_cors import CORS

app = Flask(__name__)
CORS(app)


@app.route("/", methods=['GET'])
def hello():
    return jsonify(success=True, response="hello CI!"), 200


@app.route("/ping", methods=['GET'])
def ping():
    return jsonify(success=True, response="pong!"), 200


@app.route("/hello", methods=['GET'])
def index():
    return "Hello World Testing a CI with gitlab!!"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=False)
